open Types
(* used to keep track of pointers *)
let addr_gbl = ref 0
let newref() = addr_gbl := !addr_gbl + 1; !addr_gbl

(* look for a variable in the environment *)
let rec lookup s = function
  | [] -> raise (Runtime_error "Variable not found") 
  | (id, value)::ls when id = s -> value
  | _::ls -> lookup s ls

(* functions that can be used on the store *)
let replace = Hashtbl.replace
let find = Hashtbl.find
let extend store id value = Hashtbl.add store id value; store 
let delete = Hashtbl.remove

let calls_gbl = ref 0
let reset_calls = calls_gbl := 0

(* evaluating an expression *)
let rec eval_exp env store prog = 
  calls_gbl := !calls_gbl + 1;
  match prog with
  (* No expression evaluates to a unit *)
  | Unit                    -> Val_Unit
  | Const i                 -> Val_Int i
  | Bool b                  -> Val_Bool b
  | Ref (s, i)              -> Val_Ref i
  | Readint                 -> Val_Int (read_int ())
  | Identifier s            -> lookup s env
  (* an expression followed by "Unit" should be evaluated the same as a single expression *)
  | Seq (e, Unit)           -> eval_exp env store e
  (* The last expression in a sequence is the one returned *)
  | Seq (e, e')             -> ignore (eval_exp env store e) ; eval_exp env store e'
  (* Deconstruct the while loop and evaluate the deconstruction until the conditional evaluates to false *)
  | While (e, e')           -> (match eval_exp env store e with
    | Val_Bool true                 -> eval_exp env store (Seq (e', While (e, e')))
    | Val_Bool false                -> Val_Unit
    | _                         -> raise (Runtime_error "Expected Boolean"))
  (* An expression is only evaluated if the condition matches *)
  | If (e, e', e'')         -> (match eval_exp env store e with 
    | Val_Bool true                 -> eval_exp env store e'
    | Val_Bool false                -> eval_exp env store e''
    | _                         -> raise (Runtime_error "Expected Boolean")) 
  (* Assignment is global declaration for now *)
  | Asg (e, e')             -> (match eval_exp env store e with
    | Val_Ref id                    -> let new_val = eval_exp env store e' in replace store id new_val; Val_Unit
    | _                         -> raise (Runtime_error "Expected Var"))
  (* Find whatever the identifier holds (this could be another identifier *)
  | Deref e                 -> (match eval_exp env store e with
      | Val_Ref id                  -> find store id
      | _                       -> raise (Runtime_error "Expected Var"))
  (* Perform an operator if the types allow it *)
  | Operator (o, e, e')     -> (match (eval_exp env store e, eval_exp env store e') with
      | (Val_Bool b, Val_Unit)  -> if o == Not then Val_Bool (not b) else raise (Runtime_error "Expected Boolean")
      | (Val_Int i, Val_Int i') -> (match o with
          | Plus                -> Val_Int (i + i')
          | Minus               -> Val_Int (i - i')
          | Times               -> Val_Int (i * i')
          | Divide              -> Val_Int (i / i')
          | Leq                 -> Val_Bool (i <= i')
          | Geq                 -> Val_Bool (i >= i')
          | Equal               -> Val_Bool (i = i')
          | Noteq               -> Val_Bool (i != i')
          | _                   -> raise (Runtime_error "Unexpected operator for Integer type"))
      | (Val_Bool b, Val_Bool b')   -> (match o with
          | And                 -> Val_Bool (b && b')
          | Or                  -> Val_Bool (b || b')
          | Equal               -> Val_Bool (b = b')
          | Noteq               -> Val_Bool (b != b')
          | _                   -> raise (Runtime_error "Unexpected operator for Boolean type"))
      | _                   -> raise (Runtime_error "Type mismatch in operator"))
  (* if application is done with no parameters:
     if no parameters are needed evaluate the function
     if paramters are needed return the function
  *)
  | Application (e, Unit)   -> (match eval_exp env store e with
      | Val_Fun ([], fun_env, exp)  -> eval_exp (fun_env@env) store exp
      | Val_Fun (xs, fun_env, exp)  -> Val_Fun (xs, fun_env, exp)
      | _ -> raise (Runtime_error "Expected function"))
  (* If you are on the last parameter then add it to the environment and evaluate the function
     Functions contain an environment that their parameters get added to, they also inherit the environment that they are called in *)
  | Application (e, e')     -> (match eval_exp env store e with 
      | Val_Fun (x::[], fun_env, exp)        -> let v = eval_exp env store e' in
                                                eval_exp (((x,v)::fun_env)@env) store exp
      | Val_Fun (x::xs, fun_env, exp)        -> let v = eval_exp env store e' in
                                                Val_Fun (xs, ((x,v)::fun_env), exp)
      | _                       -> raise (Runtime_error "Expected function"))
  | Fun (n, xs, p, exp) -> Val_Fun (xs, fun_val env store p, exp)
  (* printing an integer will return a unit type *)
  | Printint e              -> (match eval_exp env store e with
      | Val_Int i                   -> print_int i; print_newline (); Val_Unit 
      | _                       -> raise (Runtime_error "Expected Integer"))
  (* let is for constants, keep them on the stack (env) *)
  | Let (s, e, e')          -> let v = eval_exp env store e in
                               eval_exp ((s, v)::env) store e'
  (* new is for pointers, generate an id for the pointer to keep in the env and then update the store with this id and the thing the pointer should point to *)
  | New (s, e, e')          -> let v = eval_exp env store e in
                               let l = newref() in
                               let v' = eval_exp ((s, Val_Ref l)::env) (extend store l v) e'
                               in delete store l; v'
and fun_val env store = function
  | [] -> []
  | (s, exp)::xs -> (s, eval_exp env store exp)::(fun_val env store xs)


(* recurse until the main function is found and then evaluate it, if it can't be found raise an error *)
let rec eval_functions env = function
  | []                          -> raise (Runtime_error "No main function found")
  | (n,_,e)::xs when n = "main" -> eval_exp env (Hashtbl.create 25) e
  | (n,p,e)::xs                 -> let func = Val_Fun (p,[], e) 
                                   in eval_functions ((n, func)::env) xs
let eval_prog prog = ((eval_functions [] prog), !calls_gbl)

