open Types

(* used to keep track of pointers *)
let addr_gbl = ref 0
let newref() = addr_gbl := !addr_gbl + 1; !addr_gbl



(* functions that can be used on the store *)
let clear = Hashtbl.clear
let replace = Hashtbl.replace
let find store id = if Hashtbl.mem store id then
    Some (Hashtbl.find store id)
  else
    None
let extend store id value = Hashtbl.add store id value; store 
let delete = Hashtbl.remove

(* Define a function that returns whether an expression will have a side effect *)
let side_effect = function
  | Const _ -> false
  | Bool _ -> false
  | Ref _ -> false
  | Fun _ -> false
  | _ -> true

(* Find a variable *)
let rec lookup s = function
  | [] -> None
  | (id, value)::ls when id = s -> Some value
  | _::ls -> lookup s ls
                                    

let rec opt_exp env store exp = match exp with
  (* for some expressions you can't optimise *)
  | Unit -> exp
  | Const _ -> exp
  | Bool _ -> exp
  | Fun _ -> exp
  | Ref _ -> exp
  | Readint -> exp
  (* with an identifier try to find it (constant propagation, otherwise just return the expression *)
  | Identifier s -> ( match lookup s env with
      | None -> exp
    | Some v -> v)
  (* You can replace a sequence ending in a unit with just the optimised first expression*)
  | Seq (e, Unit) -> opt_exp env store e
  (* for any other sequence, optimise the expressions in place *)
  | Seq (e, e') -> let exp = opt_exp env store e in
    let exp' = opt_exp env store e' in
    if exp = Unit then exp' else Seq (exp, exp')
  (* loop unrolling *)
  | While (e, e') -> ( match opt_exp env store e with
    (* if the gate evaluated to true unroll once *)
    | Bool true -> opt_exp env store (Seq (e', exp))
    (* if the gate evaluates to false then end unrolling *)
    | Bool false -> Unit
    (* if it's not a boolean then it probably can't be determined at this point so don't unroll, also get rid of all mutable variables as they could have been changed in the while loop *)
    | _ -> let ret = While (e, opt_exp env store e') in
                        clear store; ret)
  (* see if the if statement can be removed *)
  | If (e, e', e'') -> ( match opt_exp env store e with
    (* if the gate is always true replace the if with optimised first expression*)
    | Bool true -> opt_exp env store e'
    (* if the gate is always false replace it with the second expression *)
    | Bool false -> opt_exp env store e''
    (* otherwise optimise the insides of the if *)
    | gate -> let ret =
                If (gate, opt_exp env store e', opt_exp env store e'') in
      clear store; ret ) 
  (* Asg never gets optimised out *)
  | Asg (e, e') -> ( match opt_exp env store e with
      | Ref (s, id) -> let new_val = opt_exp env store e' in replace store id new_val; Asg (Ref (s, id), new_val)
      | x -> Asg (x, opt_exp env store e') )
  (* if you can work out the value from the store replace the deref with that value *)
  | Deref e -> ( match opt_exp env store e with
      | Ref (s, id) -> ( match  find store id with
        | Some v -> if side_effect v then Deref (Ref (s, id)) else v
        | None -> Deref (Ref (s, id)) )
      | x -> Deref x )
  (* Function application is tricky
     applying a unit means no more parameters *)
  | Application (e, Unit) -> ( match opt_exp env store e with
      (* If your applying to a function and all the parameters have been accounted for you can inline the function *)
      | Fun (n, [], fun_env, expr) -> opt_exp (fun_env@env) store expr
      (* If there are any variables left to add you can make a closure *)
      | Fun (n, xs, fun_env, expr) -> Fun (n, xs, fun_env, expr)
      (* If it's not a function that's being returned leave it *)
      | x -> let ret = Application(x, Unit) in clear store; ret)
  (* an application that is not finished *)
  | Application (e, e') -> ( match opt_exp env store e with
      (* if it's the last variable then inline the function *)
      | Fun (_, x::[], fun_env, expr) -> let v = opt_exp env store e' in
        opt_exp (((x,v)::fun_env)@env) store expr
      (* make a closure *)
      | Fun (n, x::xs, fun_env, expr) -> let v = opt_exp env store e' in
        Fun (n, xs, ((x,v)::fun_env), expr) 
      (* if it's not a function don't do anything *)
      | x -> let ret = Application(x, opt_exp env store e') in
                        clear store; ret)
  (* optimise the print *)
  | Printint e -> Printint (opt_exp env store e)
  (* if the gate has a side effect keep the let otherwise get rid of it *)
  | Let (s, e, e') -> let v = opt_exp env store e in
                      if side_effect v then 
                        Let (s, opt_exp env store e, opt_exp env store e')
                      else
                        opt_exp ((s, v)::env) store e'
  (* always keep new *)
  | New (s, e, e') -> let v = opt_exp env store e in
                      let l = newref() in
                      let exp = New(s, v, opt_exp ((s, Ref (s, l))::env) (extend store l v) e')
                      in delete store l; exp
  (* evaluate the operator *)
  | Operator (o, e, e') -> let left = opt_exp env store e in
                           let right = opt_exp env store e' in
                           match (left, right) with
      | (Bool b, Unit) -> if o == Not then Bool (not b) else Operator (o, left, right)
      | (Const i, Const i') -> ( match o with
          | Plus -> Const (i + i')
          | Minus -> Const (i - i')
          | Times -> Const (i * i')
          | Divide -> Const (i / i')
          | Leq -> Bool (i <= i')
          | Geq -> Bool ( i >= i')
          | Equal -> Bool (i = i') 
          | Noteq -> Bool (i != i')
          | op -> Operator (op, left, right) )
      | (Bool b, Bool b') -> ( match o with
          | And -> Bool (b && b')
          | Or -> Bool (b || b')
          | Equal -> Bool (b = b')
          | Noteq -> Bool (b != b')
          | op -> Operator (op, left, right) )
      | _ -> Operator (o, left, right)

(* optimise every function one after the other *)
let rec opt_functions env store acc = function
  | [] -> List.rev acc
  | (n, p, e)::xs -> let func = Fun (n, p,[],e) in
    let new_env = ((n,func)::env) in
    opt_functions new_env store ((n, p, (opt_exp env store e))::acc) xs

(* to optimise a program one must optimise all of its functions *)
let opt_prog prog = opt_functions [] (Hashtbl.create 25) [] prog
