open Lexer
open Lexing
open Printf

(* parse with error will catch any lexing or parsing errors and print the position and lexbuf involved *)
let parse_with_error lexbuf =
  try Parser.main Lexer.read lexbuf with
  | SyntaxError err -> eprintf "Offset %d: %s\n%!"
                         lexbuf.lex_curr_p.pos_cnum
                         err;
                       exit (-1)
  | Parser.Error    -> eprintf "Offset %d: parser error at %s.\n%!" 
                         lexbuf.lex_curr_p.pos_cnum
                         (Lexing.lexeme lexbuf);
                       exit (-1)

let eval_with_error program =
  try Evaluator.eval_prog program with
  | Types.Unimplemented_error s -> eprintf "Runtime Error: %s is not yet implemented.\n%!" s;
                                   exit (-1)
  | Types.Runtime_error s -> eprintf "Runtime Error: %s\n%!" s;
                             exit (-1) 

let _  =
  match Sys.argv |> Array.to_list |> List.rev with
  | [] -> failwith "What are you even doing?"
  | n::[] -> failwith "Usage ex.native filename" 
  | filename::flags -> (* open file parse it and print the AST *)
    let evaluate prog = let (v, count) = eval_with_error prog in
      Types.string_of_val v ^ if List.mem "-c" flags then "\n Calls made by evaluator: " ^ string_of_int count else "" in
    let pass a = a in
    let ic = open_in filename in
        ic
        |> Lexing.from_channel
        |> parse_with_error 
        |> (if List.mem "-o" flags then Optimiser.opt_prog else pass)
        |> (if List.mem "-p" flags then Types.string_of_program
            else if List.mem "-i" flags then Compiler.interpret_program
            else if List.mem "-x" flags then Compiler.compilex86_program
            else evaluate)
        |> print_string;
        close_in ic;
    print_newline ()
