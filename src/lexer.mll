{
    open Parser
    exception SyntaxError of string
}

let int = ['0'-'9']+
let white = [' ' '\t']+
(* an identifier must begin with a character and can then contain any number of characters, numbers or underscores *)
let str = ['a'-'z' 'A'-'Z']['a'-'z' 'A'-'Z' '0'-'9' '_']*
let newline = '\r' | '\n' | "\r\n"
(* a comment is a // followed by any number of characters that are not newlines followed by a newline *)
let line_comment = "//"[^'\r' '\n']*(newline)

rule read = parse
    | white             { read lexbuf }
    | newline           { read lexbuf }
    | line_comment      { read lexbuf }
    | int               { INT (int_of_string (Lexing.lexeme lexbuf)) }
    | "var"             { VAR }
    | "val"             { VAL }
    | "print_int"       { PRINTINT }
    | "read_int"        { READINT }
    | "return"          { RET }
    | "while"           { WHILE }
    | "if"              { IF }
    | "else"            { ELSE }
    | "!="              { NOTEQ }
    | "<="              { LEQ }
    | ">="              { GEQ }
    | "=="              { EQUAL } 
    | "||"              { OR }
    | "&&"              { AND }
    | '~'               { NOT }
    | '!'               { DEREF }
    | '='               { ASSIGN }
    | '{'               { LPAREN }
    | '}'               { RPAREN }
    | '('               { LBRACKET }
    | ')'               { RBRACKET }
    | ','               { COMMA }
    | ';'               { SEMICOLON }
    | '+'               { PLUS }
    | '-'               { MINUS }
    | '*'               { TIMES }
    | '/'               { DIVIDE }
    | str               { IDENTIFIER (Lexing.lexeme lexbuf)}
    | eof               { EOF }
    |  _                { raise (SyntaxError ("Unexpected char: " ^ Lexing.lexeme lexbuf)) }
