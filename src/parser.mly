%{
    open Types
%}
%token PLUS MINUS TIMES DIVIDE 
%token LEQ GEQ EQUAL NOTEQ
%token AND OR NOT
%left ELSE
%left RBRACKET
%left ASSIGN
%left RET
%left OR 
%left AND
%left EQUAL NOTEQ
%left LEQ GEQ
%left PLUS MINUS
%left TIMES DIVIDE 
%left DEREF
%right NOT
%left LBRACKET
%token <int> INT
%token WHILE IF ELSE
%token RET DEREF
%token ASSIGN
%token LPAREN RPAREN
%token LBRACKET RBRACKET
%token PRINTINT READINT
%token <string> IDENTIFIER
%token COMMA SEMICOLON EOF
%token VAR VAL
%start <Types.program> main

%%

(* a program is a list of function definitions *)
main :
    | fl = list(fundef); EOF   {fl}

(* a function definition is an identifier a list of parameters and a list of expressions *)
fundef :
    | f = IDENTIFIER; LBRACKET; l = separated_list(COMMA, param); RBRACKET; e = expression;  {(f,l,e)}

(* a paramater is an identifier *)
param :
    | n = IDENTIFIER {n}

(* the end of a list can either be a new scope (a let or new) or an empty expression *)
exp_list :
    | (* empty *) { Unit } 
    | v = new_var; SEMICOLON; l = exp_list { New (fst v, snd v, l) }
    | v = new_val; SEMICOLON; l = exp_list { Let (fst v, snd v, l) }
    | e = semicolon; SEMICOLON; l = exp_list { Seq (e,l) }
    | e = no_semicolon; l = exp_list { Seq (e,l) }

(* expressions that shouldn't end in a semicolon *)
no_semicolon :
    | exp = while_loop { exp }
    | exp = if_then { exp }
    | LPAREN; e = exp_list; RPAREN { e }

(* expressions that should end in a semicolon *)
semicolon :
    | LBRACKET; e =  expression; RBRACKET { e }
    | exp = assign; { exp } 
    | exp = return; { exp } 
    | exp = fn_call; { exp } 
    | exp = INT { Const exp }
    | DEREF exp = expression { Deref exp }
    | exp = id { exp }
    | v = expression; o = operator; vv = expression { Operator (o,v,vv) }
    | NOT; v = expression { Operator (Not, v, Unit) }

(* all expressions *)
expression :
    | e = semicolon { e }
    | e = no_semicolon { e }

(* comma separated list of expressions to be used in function calls *)
exp_comma_list :
    | l = separated_list(COMMA, expression) { l }
    
(* inline operators prevent shift reduce, this was in the ocaml manual *)
%inline operator :
    | o = arithmetic_op { o }
    | o = logical_op { o }
    | o = conditional_op { o }

%inline arithmetic_op :
    | PLUS   { Plus }
    | MINUS  { Minus }
    | TIMES  { Times }
    | DIVIDE { Divide }

%inline logical_op :
    | AND   { And }
    | OR    { Or }

%inline conditional_op :
    | EQUAL { Equal }
    | NOTEQ { Noteq } 
    | LEQ   { Leq }
    | GEQ   { Geq }

(* this is an identifier *)
id :
    | i = IDENTIFIER { Identifier i }

(* assigning to an expression *)
assign :
    | n = expression; ASSIGN;  v = expression {Asg (n, v)}
    
(* the different kinds of functions *)
fn_call :
    | f = read_int { f }
    | f = print_int { f }
    | e = expression; LBRACKET; vs = exp_comma_list; RBRACKET { application e (List.rev vs) }

(* it would be nice to get rid of the parens for while and if (just have an expression instead) but this introduced a single shift reduce error that I couldn't fix in the time frame given *)
while_loop :
    | WHILE; LBRACKET v = expression; RBRACKET; exp = expression; { While (v, exp)}

if_then :
    | IF; LBRACKET v = expression; RBRACKET; e = expression; ELSE ee = expression { If (v, e, ee)}

(* these are inline because it was easier to change them quickly this way *)
print_int :
    | PRINTINT; LBRACKET; v = expression; RBRACKET; { Printint v } 

read_int :
    | READINT; LBRACKET; RBRACKET;{ Readint }

return :
    | RET v  = expression { v } 
(* mutable datatype (NEW) *)
new_var :
    | VAR; i = IDENTIFIER; ASSIGN; v = expression { (i, v) }
(* immuatable dataype (LET) *)
new_val :
    | VAL; i = IDENTIFIER; ASSIGN; v = expression { (i, v) }
