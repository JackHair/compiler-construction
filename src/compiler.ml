open Types
open Hashtbl
let ram = Hashtbl.create 100
let acc = ref 0
let sp = ref 0
let new_addr () = sp := !sp + 1; !sp

(* Find a variable in a list *)
let rec search s = function
  | [] -> failwith ("Couldn't find variable: " ^ s)
  | (s', addr)::ls when s = s' -> addr
  | _::ls -> search s ls

let fun_of_op = function
  | Plus   -> ( + )
  | Minus  -> ( - )
  | Times  -> ( * )
  | Divide -> ( / )
  | Equal  -> (fun x y -> if x = y then 1 else 0)
  | Noteq  -> (fun x y -> if x != y then 1 else 0) 
  | Leq    -> (fun x y -> if x <= y then 1 else 0)
  | Geq    -> (fun x y -> if x >= y then 1 else 0)
  | And    -> (fun x y -> if x = 1 && y = 1 then 1 else 0)
  | Or     -> (fun x y -> if x = 1 || y = 1 then 1 else 0)
  | Not    -> (fun x _ -> if x = 1 then 0 else 1)

let instruction_of_op = function
  | Plus   -> "add"
  | Minus  -> "sub"
  | Times  -> "mul"
  | Divide -> "div"
  | Equal  -> "eq"
  | Noteq  -> "neq"
  | Leq    -> "leq"
  | Geq    -> "geq"
  | And    -> "and"
  | Or     -> "or"
  | Not    -> "not"

(* perform an operation on two registers, putting the result in the accumulator *)
let op (op, addr, addr') =
  acc := (fun_of_op op) (find ram addr) (find ram addr')
(* put acc into memory *)
let st addr = replace ram addr !acc
(* load constant *)
let ldc n = acc := n
(* different moves for different pointer combinations *)
let mv addr' addr = replace ram addr (find ram addr')
let mv_left_pointer addr' addr = replace ram (find ram addr) (find ram addr')
let mv_right_pointer addr' addr = replace ram addr (find ram (find ram addr'))

(* Buffer to put the code in *)
let code = Buffer.create 100
(* line number *)
let line = ref 0
(* code generation of a toy assembly language *)
(* operations to asm *)
let codegen_op (op, addr, addr') = line := !line + 1; (string_of_int !line) ^ ": " ^
  (instruction_of_op op)
  ^ " a" ^ (string_of_int addr)
  ^ ", a" ^ (string_of_int addr')
  ^ "\n" |> Buffer.add_string code 
(* storing a value *)
let codegen_st addr =  line := !line + 1; (string_of_int !line) ^ ": " ^
  "st a" 
  ^ (string_of_int addr)
  ^ "\n" |> Buffer.add_string code
(* loading a constant int *)
let codegen_ldc n =  line := !line + 1; (string_of_int !line) ^ ": " ^
  "ld "
  ^ (string_of_int n)
  ^ "\n" |> Buffer.add_string code
(* various moves *)
let codegen_mv addr' addr = line := !line + 1; (string_of_int !line) ^ ": " ^
  "mv !a"
  ^ (string_of_int addr')
  ^ ", a" ^ (string_of_int addr)
  ^ "\n" |> Buffer.add_string code
let codegen_mv_right_pointer addr' addr= line := !line + 1; (string_of_int !line) ^ ": " ^
  "mv !!a"
  ^ (string_of_int addr')
  ^ ", a" ^ (string_of_int addr) 
  ^ "\n" |> Buffer.add_string code
let codegen_mv_left_pointer addr addr' = line := !line + 1; (string_of_int !line) ^ ": " ^
  "mv !a"
  ^ (string_of_int addr')
  ^ ", !a" ^ (string_of_int addr)
  ^ "\n" |> Buffer.add_string code
 
(* interpreter *)
let rec interpret symt = function
  (* perform the operation *)
  | Operator (oper, e, e') -> 
    let addr = interpret symt e in
    let addr' = interpret symt e' in
    op (oper, addr, addr');
    st addr;
    sp := addr;
    addr
  (* load the constant *)
  | Const n -> 
    let addr = new_addr () in
    ldc n;
    st addr;
    addr
  (* intepret the variable and add it to the symbol table *)
  | Let (s, e, e') ->
    let addr = interpret symt e in
    let addr' = interpret ((s, addr)::symt) e' in
    mv addr' addr;
    sp := addr;
    addr
  (* find the indentifier in the symbol table *)
  | Identifier s ->
    let addr = search s symt in
    let addr' = new_addr () in
    mv addr addr';
    addr'
  (* create a pointer *)
  | New (s, e, e') ->
    let addr = interpret symt e in
    let addr' = new_addr () in
    let ptr = new_addr () in
    mv addr ptr;
    ldc ptr;
    st addr';
    let addr'' = interpret ((s, addr')::symt) e' in
    mv addr'' addr; 
    sp := addr;
    addr
  (* lookup where the pointer is stored and move what is stored in the pointer to an address *)
  | Deref e ->
    let addr = interpret symt e in
    let addr' = new_addr () in
    mv_right_pointer addr addr';
    addr'
  (* store a value i e'in the memory pointed to by pointer e *)
  | Asg (e, e') ->
    let addr = interpret symt e in
    let addr' = interpret symt e' in
    mv_left_pointer addr' addr;
    sp := addr;
    addr
  (* a unit value returns -1 in the interpreter *)
  | Unit -> -1
  (* Sequences, do one after the other *)
  | Seq (e, Unit) -> interpret symt e
  | Seq (e, e') ->
    ignore (interpret symt e);
    interpret symt e'
  (* ifs become jumps *)
  | If (gate, e, e') ->
    let addr = interpret symt gate in
    let addr' = jmpz symt addr e e' in
    mv addr' addr;
    sp := addr;
    addr
  (* whiles become jumps *)
  | While (gate, e) ->
    let addr = interpret symt gate in
    ignore (jmpz symt addr (Seq(e, While (gate, e))) Unit);
    sp := addr - 1;
    -1
  | _ -> failwith "Unsupported Expression Type"
and jmpz symt gate e e' = if (find ram gate) = 1 then (interpret symt e) else interpret symt e'

(* this generates a made up assembly language very similar to the interpreter *)
let rec codegen symt = function
  | Operator (oper, e, e') -> 
    let addr = codegen symt e in
    let addr' = codegen symt e' in
    codegen_op (oper, addr, addr');
    codegen_st addr;
    sp := addr;
    addr
  | Const n -> 
    let addr = new_addr () in
    codegen_ldc n;
    codegen_st addr;
    addr
  | Let (s, e, e') ->
    let addr = codegen symt e in
    let addr' = codegen ((s, addr)::symt) e' in
    codegen_mv addr' addr;
    sp := addr;
    addr
  | Identifier s ->
    let addr = search s symt in
    let addr' = new_addr() in
    codegen_mv addr addr';
    addr'
  | New (s, e, e') ->
    let addr = codegen symt e in
    let ptr = new_addr () in
    let addr' = new_addr () in
    codegen_mv addr ptr;
    codegen_ldc ptr;
    codegen_st addr';
    let addr'' = codegen ((s, addr')::symt) e' in
    codegen_mv addr'' addr; 
    sp := addr;
    addr
  | Deref e ->
    let addr = codegen symt e in
    let addr' = new_addr () in
    codegen_mv_right_pointer addr addr';
    addr'
  | Asg (e, e') ->
    let addr = codegen symt e in
    let addr' = codegen symt e' in
    codegen_mv_left_pointer addr' addr;
    sp := addr;
    addr
  | Unit -> -1
  | Seq (e, Unit) -> codegen symt e
  | Seq (e, e') ->
    ignore (codegen symt e);
    codegen symt e'
  (* while and if are hard *)
  | If (gate, e, e') ->
    let addr = codegen symt gate in
    codegen_if addr symt e e';
    sp := addr;
    addr
  | While (gate, e) ->
    let before_while = !line in
    let addr = codegen symt gate in
    codegen_while before_while addr symt e;
    sp := addr - 1;
    -1
  | _ -> failwith "Unsuported Expression Type"
and codegen_while prev_line gate_addr symt e =
  (* use temporary buffer to store old code generate the gate and then add the jumps to ake it into a loop*)
  let tmp = Buffer.create 100 in
  Buffer.add_buffer tmp code;
  Buffer.clear code;
  line := !line + 1;
  let jmpz_line = !line in
  ignore (codegen symt e);
  string_of_int jmpz_line ^ ": " ^
  "jmpz a" ^ string_of_int gate_addr 
  ^  ", " ^ string_of_int (!line + 2) ^ "\n" |> Buffer.add_string tmp;
  Buffer.add_buffer tmp code;
  line := !line + 1;
  string_of_int !line ^ ": " ^ "jmp " ^ string_of_int (prev_line + 1) ^ "\n" |> Buffer.add_string tmp;
  Buffer.clear code;
  Buffer.add_buffer code tmp
(* if, generate two lots of code, mesh them together with jumps so that either can occur based on the gate *)
and codegen_if gate_addr symt e e' =
  let tmp = Buffer.create 100 in
  Buffer.add_buffer tmp code;
  Buffer.clear code;
  line := !line + 1;
  let jmpz_line = !line in
  let addr = codegen symt e in
  codegen_mv addr gate_addr;
  string_of_int jmpz_line ^ ": " ^
  "jmpz a" ^ string_of_int gate_addr
  ^ ", " ^ string_of_int (!line + 2) ^ "\n" |> Buffer.add_string tmp;
  Buffer.add_buffer tmp code;
  Buffer.clear code;
  line := !line + 1;
  let jmp_line = !line in
  let addr = codegen symt e' in
  codegen_mv addr gate_addr;
  string_of_int jmp_line ^ ": " ^ "jmp " ^ string_of_int (!line + 1) ^ "\n" |> Buffer.add_string tmp;
  Buffer.add_buffer tmp code;
  Buffer.clear code;
  Buffer.add_buffer code tmp

let interpret_function  oper = let addr = interpret [] oper in find ram addr |> string_of_int

let compile_function oper = 
  Buffer.reset code; 
  line := 0;
  let addr = codegen [] oper in 
  Buffer.output_buffer stdout code;
  line := !line + 1;
  string_of_int !line ^ ": " ^ "ld a" ^ (string_of_int addr)

let rec interpret_program = function
  | [] -> failwith "no main"
  | (n, p, e)::xs when n = "main" -> interpret_function e
  | _::xs -> interpret_program xs

let rec compile_program = function
  | [] -> failwith "no main"
  | (n, p, e)::xs when n = "main" -> compile_function e
  | _::xs -> compile_program xs

(* time for x86 *)
(* all of the operators need to be translated *)
(* true is 1 false is zero *)
let instx86_of_operator = function
  | Plus   -> "addq %rbx, %rax\n"
  | Minus  -> "subq %rbx, %rax\n"
  | Times  -> "imulq %rbx, %rax\n"
  (* dividing takes two registers for the original, for our purposes zero out the upper half of the number with xor *) 
  | Divide -> "xor %rdx, %rdx\n" ^ "cqo\n"^ "idivq %rbx\n"
  (* compare the two values, and set al (an 8 bit register) based on what you want to know, move from the 8 bit register to a 64 bit one remembering to zero extend *)
  | Equal  -> "cmpq %rbx, %rax\n" ^ "sete %al\n" ^ "movzbq %al, %rax\n"
  | Noteq  -> "cmpq %rbx, %rax\n" ^ "setne %al\n" ^ "movzbq %al, %rax\n"
  | Leq    -> "cmpq %rbx, %rax\n" ^ "setle %al\n" ^ "movzbq %al, %rax\n"
  | Geq    -> "cmpq %rbx, %rax\n" ^ "setge %al\n" ^ "movzbq %al, %rax\n"
  | And    -> "and %rbx, %rax\n"
  | Or     -> "or %rbx, %rax\n"
  (* not is unary *)
  | Not    -> "test %rax, %rax\n" ^ "setz %al\n" ^ "movzbq %al, %rax\n" 

(* all of the previous codegen code can be adapted so that it produces x86 *)
let codegenx86_op op =
  "popq %rbx\n" ^
  "popq %rax\n" ^
  (instx86_of_operator op) ^
  "pushq %rax\n" |> Buffer.add_string code

let codegenx86_id addr =
  "//offset " ^ (string_of_int addr) ^ "\n" ^
  "movq " ^ (-8 * addr |> string_of_int) ^ "(%rbp), %rax\n" ^
  "pushq %rax\n" |> Buffer.add_string code

let codegenx86_st n =
  "pushq $" ^ (string_of_int n) ^ "\n" |> Buffer.add_string code

let codegenx86_let _ =
  "popq %rax\n" ^
  "popq %rbx\n" ^
  "pushq %rax\n" |> Buffer.add_string code

let codegenx86_new _ =
  "popq %rax\n" ^
  "popq %rbx\n" ^
  "popq %rbx\n" ^
  "pushq %rax\n" |> Buffer.add_string code

let codegenx86_deref _ =
  "popq %rax\n" ^
  "movq (%rax), %rbx\n" ^
  "pushq %rbx\n" |> Buffer.add_string code

(* load the address of a location in the stack *)
let codegenx86_ptr _ =
  "leaq " ^ (-8 * !sp |> string_of_int) ^ "(%rbp), %rax\n" ^
  "pushq %rax\n" |> Buffer.add_string code

let codegenx86_asg _ =
  "popq %rax\n" ^
  "popq %rbx\n" ^
  (* move rax into the memory pointed to by rbx *)
  "movq %rax, (%rbx)\n" |> Buffer.add_string code

let codegenx86_unit _ =
  "pushq $0\n" |> Buffer.add_string code

(* x86 uses labels for jumping to *)
let label_num = ref 3
(* need to store the number of parameters that are currently on the stack for function calling *)
let param_num = ref 0
(* list of functions in the program *)
let fun_list = ref []

(* check something is in a list *)
let rec is_in n = function
  | [] -> false
  | x::xs -> n = x || is_in n xs

(* turn a parameter number into a place to store the parameter (standard linux function call conventions) *)
let to_addr = function
  | 0 -> "%rdi"
  | 1 -> "%rsi"
  | 2 -> "%rdx"
  | 3 -> "%rcx"
  | 4 -> "%r8"
  | 5 -> "%r9"
  | n -> ((8 * (n - 4)) |> string_of_int) ^ "(%rbp)"

(* pop parameters before a function is called *)
let rec pop_params max = function
  | n when n < max && n < 6 && n >= 0 ->
    "popq " ^ (to_addr n) ^ "\n" |> Buffer.add_string code;
    sp := !sp - 1;
    pop_params max (n + 1)
  | _ -> ()

(* time for the pattern match on all of the expression types *)
(* every expression when evaluated will push to the stack a return value *)
let rec codegenx86 symt = function
  (* push value for the first expression, push value for the second, generate the code for the operator *)
  | Operator (op, e1, e2) ->
    codegenx86 symt e1;
    codegenx86 symt e2;
    codegenx86_op op;
    (* must keep a track of the stack *)
    sp := !sp - 1
  (* if an identifier is a function *)
  | Identifier x when is_in x !fun_list ->
    (* load the address of the function and add it to the stack *)
    "leaq (" ^ x ^ "), %rax\n" ^
    "push %rax\n" |> Buffer.add_string code;
    sp := !sp + 1
  (* if it's an identifier or a reference to a variable find it in the symbol table *)
  | Identifier x | Ref (x, _) ->
    let addr = search x symt in
    codegenx86_id (addr);
    sp := !sp + 1
  (* store the constant *)
  | Const n ->
    codegenx86_st n;
    sp := !sp + 1
  (* true is represented by pushing a 1 *)
  | Bool b when b = true ->
    codegenx86_st 1;
    sp := !sp + 1
  (* false is represented by pushing a 0 *)
  | Bool b when b = false ->
    codegenx86_st 0;
    sp := !sp + 1
  (* this is all similar to codegen *)
  | Let (x, e1, e2) ->
    codegenx86 symt e1;
    codegenx86 ((x, !sp) :: symt) e2;
    codegenx86_let ();
    sp := !sp - 1
  | Seq (e, Unit) ->
    codegenx86 symt e
  | Seq (e, e') ->
    codegenx86 symt e;
    sp := !sp -1;
    (* must ignore the value of the first thing in the sequence by poppng it *)
    "popq %rax\n" |> Buffer.add_string code;
    codegenx86 symt e'
  | New (s, e, e') ->
    (* put the value on the stack *)
    codegenx86 symt e;
    (* put a pointer to the value onto the stack *)
    codegenx86_ptr ();
    sp := !sp + 1;
    codegenx86 ((s, !sp) :: symt) e';
    codegenx86_new ();
    sp := !sp - 2
  | Deref e ->
    codegenx86 symt e;
    codegenx86_deref ()
  | Asg (e, e') ->
    codegenx86 symt e;
    codegenx86 symt e';
    codegenx86_asg ();
    (* assignment returns a unit 0 *)
    codegenx86_unit ();
    sp := !sp - 1
  | Unit ->
    (* unit just pushed zero onto the stack *)
    codegenx86_unit ();
    sp := !sp + 1
  (* whiles rely on labels and jumping to the correct labels *)
  | While (gate, e) ->
    let start = !label_num in
    label_num := !label_num + 1;
    (* starting label, before the gate test*)
    ".L" ^ string_of_int start ^ ":\n" |> Buffer.add_string code;
    codegenx86 symt gate;
    (* finish is the label after the while loop *)
    let finish = !label_num in
    label_num := !label_num + 1;
    (* do the gate test and jump to the end if it was false *)
    "popq %rax\n" ^ "test %rax, %rax\n" ^ "jz .L" ^ string_of_int finish ^ "\n" |> Buffer.add_string code;
    sp := !sp - 1;
    (* generate the body of the while *)
    codegenx86 symt e;
    (* jump back to the gate and repeat *)
    "popq %rax\n" ^ "jmp .L" ^ string_of_int start ^ "\n" |> Buffer.add_string code;
    ".L" ^ string_of_int finish ^ ":\n" |> Buffer.add_string code;
    codegenx86_unit();
    sp := !sp + 1
  (* if also relys on labels and jumping *)
  | If (gate, e, e') ->
    let else_lbl = !label_num in
    label_num := !label_num + 1;
    codegenx86 symt gate;
    (* do the gate test and jump to the else if it was false *)
    "popq %rax\n" ^ "test %rax, %rax\n" ^ "jz .L" ^ string_of_int else_lbl ^ "\n" |> Buffer.add_string code;
    sp := !sp - 1;
    let end_if = !label_num in
    label_num := !label_num + 1;
    (* generate the first block of the if *)
    codegenx86 symt e;
    (* jump past the else, add the label for the start of the else *)
    "jmp .L" ^ string_of_int end_if ^ "\n" ^ ".L" ^ string_of_int else_lbl ^ ":\n" |> Buffer.add_string code;
    sp := !sp - 1;
    codegenx86 symt e';
    (* add the label after the else *)
    ".L" ^ string_of_int end_if ^ ":\n" |> Buffer.add_string code
  (* function call *) 
  | Application (e, Unit) ->
    (* before generating code save the parameter number and reset it *)
    let tmp_param_num = !param_num in
    param_num := 0;
    codegenx86 symt e;
    param_num := tmp_param_num;
    (* pop the address of the function being called *)
    "popq %rax\n" |> Buffer.add_string code;
    (* get all of the parameters *)
    pop_params !param_num 0;
    (* call the function *)
    "call " ^ "*%rax" ^ "\n" ^
    (* clean up the stack *)
    "addq $" ^
        (((match (!param_num - 6) with
            | n when n < 1 -> 0
            | n -> n
        )
        * 8) |> string_of_int) ^ ", %rsp\n" ^ "pushq %rax\n" |> Buffer.add_string code;
    param_num := 0
  (* function call where not all parameters have been added *)
  | Application (func, param) ->
    (* push the parameter onto the stack and maintain the correct param nums *)
    let tmp_param_num = !param_num in
    param_num := 0;
    codegenx86 symt param;
    param_num := tmp_param_num + 1;
    codegenx86 symt func
  | Printint e ->
    (* printing an integer with the inbuilt print function *)
    codegenx86 symt e;
    "popq %rdi\n" ^ "call print\n" |> Buffer.add_string code;
    codegenx86_unit()
  | Readint ->
    (* reading an integer with the inbuilt read_int function *)
    "call read_int\n" ^
    "pushq %rax\n" |> Buffer.add_string code;
    sp := !sp + 1
    (* given a closure attempt to load its functions address *)
  | Fun (n, _, _, _) ->
     "leaq (" ^ n ^ "), %rax\n" ^
    "push %rax\n" |> Buffer.add_string code;
    sp := !sp + 1


let compilex86_main_function oper = 
  Buffer.add_string code Asm.main;
  codegenx86 [] oper;
  Buffer.add_string code "popq %rax"

(* code to generate loading the parameters for a function *)
let rec load_params num = function
  | [] -> []
  | p::ps ->
    "pushq " ^ (to_addr num) ^ "\n" |> Buffer.add_string code;
    sp := !sp + 1;
    let addr = !sp in
    (p, addr) :: (load_params (num + 1) ps)

(* wrap the correct labels around the function *)
let compilex86_function (n, p, e) =
  ".globl " ^ n ^ "\n" ^ ".type " ^ n ^ ", @function\n" ^
  n ^ ":\n"
  ^ "pushq %rbp\n" ^ "movq %rsp, %rbp\n" |> Buffer.add_string code;
  let symt = load_params 0 p in
  codegenx86 symt e;
  (* leave does the whole pop %rbp thing*)
  "popq %rax\n" ^ "leave\n" ^ "ret\n" |> Buffer.add_string code

(* compile all of the functions until you hit main *)
let rec compilex86_functions = function
  | [] -> failwith "no main"
  | (n, p, e)::xs when n = "main" -> compilex86_main_function e
  | x::xs ->
    let tmp_sp = !sp in
    sp := 0;
    compilex86_function x;
    sp := tmp_sp;
    compilex86_functions xs

let rec list_functions = function
  | [] -> failwith "no main"
  | (n, _, _) :: xs  when n = "main" -> []
  | (n, _, _) :: xs -> n :: (list_functions xs)

(* combine the "hacked" asm with the generated asm *)
let compilex86_program prog =
  fun_list := list_functions prog;
  Buffer.reset code;
  Buffer.add_string code Asm.prefix;
  compilex86_functions prog;
  Buffer.add_string code Asm.suffix;
  (Buffer.output_buffer stdout code);
  ""

