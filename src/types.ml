exception Runtime_error of string
exception Unimplemented_error of string

type opcode =
  | Plus | Minus | Times | Divide
  | Leq | Geq | Equal | Noteq
  | And | Or | Not

type expression =
  | Seq of expression * expression (* e; e *)
  | While of expression * expression (* while e do e *)
  | If of expression * expression * expression (* if e do e else e *)
  | Asg of expression * expression (* e := e *)
  | Bool of bool
  | Fun of string * string list * (string * expression) list * expression
  | Deref of expression (* !e *)
  | Operator of opcode * expression * expression (* e + e *)
  | Application of expression * expression (* e(e) *)
  | Const of int (* 7 *)
  | Readint (* read_int () *)
  | Printint of expression (* print_int (e) *)
  | Identifier of string (* x *)
  | Ref of string * int
  | Let of string * expression * expression (* let x = e in e *)
  | New of string * expression * expression (* new x = e in e *)
  | Unit

type fundef = string * string list * expression
type program = fundef list

(* an expression can evaluate to a boolean, an integer, the empty unit or an identifier name *)
type val_type  =
  | Val_Bool of bool | Val_Int of int | Val_Ref of int | Val_Fun of string list * (string * val_type) list * expression | Val_Unit

(* turn a list of parameters into multiple function applications *)
let rec application e = function
  | [] -> Application (e, Unit)
  | x::xs -> Application (application e xs, x)

let string_of_val = function
  | Val_Bool b -> string_of_bool b
  | Val_Int i  -> string_of_int i
  | Val_Ref i  -> "Identifier"
  | Val_Unit   -> "()"
  | Val_Fun _  -> "Function"

(* function that turns an opcode into a string for printing *)
let string_of_opcode = function
  | Plus    -> "Plus"
  | Minus   -> "Minus"
  | Times   -> "Times"
  | Divide  -> "Divide"
  | Leq     -> "Leq"
  | Geq     -> "Geq"
  | Equal   -> "Equal"
  | Noteq   -> "Noteq"
  | And     -> "And"
  | Or      -> "Or"
  | Not     -> "Not"

(* function that turns an expression into a string for printing *)
let rec string_of_exp = function
  | Unit                -> "Unit"
  | Const i             -> "Const " ^ (string_of_int i)
  | Bool b              -> "Bool " ^ (string_of_bool b)
  | Readint             -> "Readint"
  | Identifier s        -> "Identifier \"" ^ s ^ "\""
  | Seq (e, e')         -> "Seq ("
                           ^ (string_of_exp e)  ^ ", "
                           ^ (string_of_exp e') ^ ")"
  | While (e, e')       -> "While (" 
                           ^ (string_of_exp e)  ^ ", "
                           ^ (string_of_exp e') ^ ")"
  | If (e, e', e'')     -> "If ("
                           ^ (string_of_exp e)   ^ ", "
                           ^ (string_of_exp e')  ^ ", "
                           ^ (string_of_exp e'') ^ ")"
  | Asg (e, e')         -> "Asg ("
                           ^ (string_of_exp e)  ^ ", "
                           ^ (string_of_exp e') ^ ")"
  | Deref e             -> "Deref ("
                           ^ (string_of_exp e) ^ ")"

  | Operator (o, e, e') -> "Operator ("
                           ^ (string_of_opcode o) ^ ", "
                           ^ (string_of_exp e)    ^ ", "
                           ^ (string_of_exp e')   ^ ")"
  | Application (e, e') -> "Application ("
                           ^ (string_of_exp e)  ^ ", "
                           ^ (string_of_exp e') ^ ")"
  | Printint e          -> "Printint ("
                           ^ (string_of_exp e) ^ ")"
  | Let (s, e, e')      -> "Let ("
                           ^ s                  ^ ", "
                           ^ (string_of_exp e)  ^ ", "
                           ^ (string_of_exp e') ^ ")"
  | New (s, e, e')      -> "New ("
                           ^ s                  ^ ", "
                           ^ (string_of_exp e)  ^ ", "
                           ^ (string_of_exp e') ^ ")"
  | Ref (s, i)          -> "Ref " ^ s
  | Fun (n,_,_,_)       -> "Function " ^ n

(* function that prints out a function definition *)
let string_of_fundef (name, parameters, exp) = "(\""
                                               ^ name                           ^ "\", [" 
                                               ^ (String.concat ", " parameters) ^ "], " 
                                               ^  (string_of_exp exp) ^ ")" 

(* function that will print a program *)
let string_of_program = function
  | [] -> "No program\n"
  | xs -> "[" ^ (String.concat "," (List.map string_of_fundef xs)) ^ "]"

