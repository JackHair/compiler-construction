let prefix = "
    .file	\"template.c\"
	.section	.rodata
.LC0:
	.string	\"%d\\n\"
	.text
    .globl print
    .type print, @function
print:
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$16, %rsp
	movl	%edi, -4(%rbp)
	movl	-4(%rbp), %eax
	movl	%eax, %esi
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	nop
	leave
	ret
.LC1:
	.string	\"%d\"
	.text
	.globl	read_int
	.type	read_int, @function
read_int:
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$16, %rsp
	leaq	-4(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	__isoc99_scanf
	movl	-4(%rbp), %eax
	leave
	ret
"
let main = "
    .globl main
    .type main, @function 
main:
	pushq	%rbp
	movq	%rsp, %rbp
"
let suffix = "
	movl	%eax, %edi
	call	print
	movl	$0, %edi
	call	exit
	.ident	\"GCC: (GNU) 6.2.1 20160830\"
	.section	.note.GNU-stack,\"\",@progbits
"
