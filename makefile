OCB_FLAGS = -use-menhir -use-ocamlfind -tag bin_annot -I src
OCB 	  = ocamlbuild $(OCB_FLAGS)
parser: src/ex.ml src/lexer.mll src/parser.mly src/types.ml
	$(OCB) ex.native
clean:
	$(OCB) -clean
test:
	make
	bash testing/test.sh
