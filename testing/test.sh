dirname=$(dirname $0)
echo "------------ Running Week 1 Tests -------------"
bash $dirname/test_helper.sh $dirname/week1
echo "------------ Running Week 2 Tests -------------"
bash $dirname/test_helper.sh $dirname/week2
echo "------------ Running Week 3 Tests -------------"
bash $dirname/test_helper.sh $dirname/week3
echo "------------ Running Week 4 Tests -------------"
bash $dirname/test_helper.sh $dirname/week4
echo "------------ Running Week 5 Tests -------------"
bash $dirname/test_helper.sh $dirname/week5
echo "------------ Running Week 6 Tests -------------"
bash $dirname/test_helper.sh $dirname/week6
echo "------------ Running Week 7 Tests -------------"
bash $dirname/test_helper.sh $dirname/week7
echo "------------ Running Week 8 Tests -------------"
bash $dirname/test_helper.sh $dirname/week8
