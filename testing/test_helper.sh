#! /bin/bash
success=0
total=0

if [ "$1" != "" ]; then
    wd=$1
else
    echo "Usage: ./test_helper.sh directory" 
    exit 0
fi
for i in $wd/programs/*.src; do
    filename=$(basename $i | cut -f1 -d '.')
    outputfile="$wd/output/$filename.o"
    exe="$(dirname $0)/../ex.native -o -x "
    output=$($exe $i 2>&1| gcc -o prog -x assembler - 2>&1 && ./prog 2>&1)
    if [ $? -ne 0 ]; then
        output="Failed"
    fi
    ((total+=1))
    if $(echo "$output" | diff $outputfile - > /dev/null); then
        echo "Test $total Passed: $filename"
        ((success+=1))
    else
        echo "Test $total Failed: $filename"
    fi
done

echo "$success out of $total tests succesfully passed."
