# Compiler Construction
## Assignment One
The first assignment is to create a linter and parser for a programming language. The tools used for this are ocaml, ocamllint and menhir, the testing is done in bash scripting, please ensure you have access to all of these when using this repository.
### Usage
1. Clone the repository `git clone https://bitbucket.org/JackHair/compiler-construction.git`
2. Change directory into the  compiler-construction folder
3. To compile into an executable file ex.native run `make`
4. To test the project run `make test`

### Syntax
* Operations where a and b are expressions.
    * Arithmetic Operators
        * Additon: `a + b`
        * Subtraction: `a - b`
        * Division: `a / b`
        * Multiplication: `a * b`
    * Logical Operators
        * AND: `a && b`
        * OR: `a || b`
    * Conditional Operators
        * Greater than or equal to: `a >= b`
        * Less than or equal to: `a <= b`
        * Not equal to: `a != b`
        * Equal to: `a == b`
* Expressions where y is an expression and x is a string starting with a character followed by any number of characters integers or underscores.
    * Code block (expression list): `{ y }`
    * While loop: `while ( y ) { y }`
    * If Else: `if ( y ) { y } else { y }`
    * Assignment: `y = 1` 
    * Dereferencing: `!y`
    * Integer: '2'
    * Operators: `(y + y) * 2`
    * Function application: `fn(y)`
    * Reading integers: `readint()`
    * Printing integers: `print_int()`
    * Variable definition: `var x = 0`
    * Constant definition; `val x = 0`

## Assignment Two
The second assignment is to evaluate the AST that has been produced by the parser and linter. This ignores function calls and just evaluates the first function defined in the program, all variables are global and are instantiated during assignment. Running a program will return a value of one of 4 types.

### Types
* Boolean: True/False
* Integer: Whole number
* Identifer: Stores any type 
* Unit: () The empty type

### Expression return behaviour
* Code block: Returns last thing in the block
* While loop: Returns  Unit type
* If Else: Returns either code block depending on the conditional
* Assignment: Returns Unit type 
* Dereferencing: Returns the value the identifier holds 
* Integer: Returns an integer type
* Operators: Returns the result of the operator
* Function Application: Unimplemented
* Reading Integers: Unimplemented
* Printing Integers: Returns the Unit type
* Variable definition: Unimplemented
* Constant definition: Unimplemented

### Errors
* Syntax error: Occurs when the lexer finds an unexpected character
* Parser error: Occurs when the parser finds an unexpected token
* Runtime error: Occurs when an expression cannot be evaluated this can be due to
    * Unimplemented error: The method of evaluating an expression has not yet been implemented.
    * No main function found: The program does not contain a main function 
    * Operator type mismatch: The expression on either side of the operator evaluate to different types.
    * Expected integer: An expression evaluated to a type other than Integer where an integer type was expected, for example calling print_int with a boolean.
    * Expected boolean: An expression evaluated to a type other than Boolean where a boolean type was expected, for example the conditional part of an if statement being `1 + 1`.
    * Expected identifier: An expression evaluated to a type other than Identifier where  an identifier type was expected, for example dereferencing a number.
    * Unexpected operator for Integer type: An operator was used that cannot be applied to integers, for example `1 || 2`.
    * Unexpected operator for Boolean type: An operator was used that cannot be applied to boolean, for example adding two boolean together.
    * Ocaml runtime errors can also occur, an example of this is when division by zero happens.

## Assignment Three
The third assignment is to evaluate the AST with function calls and local variables, both immutable (val) and mutable (var).

* Implementation
    * All functions defined before the main function are in scope inside the main function.
    * When a function is called it inherits the scope that it was called in, this has the effect of permitting both recursion and mutual recursion.
    * Var creates a mutable variable, this is a pointer and must be dereferenced.
    * Val creates an immutable variable, this does not need to be dereferenced unless it holds a pointer.

## Assignment Four
The fourth assignment is to allow the AST to be optimised before evaluation.

* Implementation
    * Constant folding: Any expressions that can be replaced with a constant are
    * Constant propagation: A variable bound in a let statement is replaced with the value bound to it as long as this value has no side effects.
    * Function inlining: When possible function application is replaced with the body of the function
    * Loop unrolling: While loops become sequences of expressions if the gate condition is known
    * Dereferencing: When a the value of a mutable variable is known to be a certain value, it is replaced with this value.

* Program flags
    * The `-p` flag can be used to print the unevaluated AST.
    * The `-c` flag can be used to print the number of steps the evaluator took.
    * The `-o` flag can be used to turn on optimisations.

## Assignment Five
The fifth assignment is to compile to a simple machine code, please see the examples in testing/week5 to view explanations of the machine code. Assignment of mutable and immutable variables, operations, if and while loops are all working.

* Program flags
    * The `-x` flag can be used to print the machine code produced
    * The `-i` flag can be used to invoke the interpreter on the ast

## Assignment Six
The six assignment is to modify the -x flag so that x86 code is produced, this can then be compiled with gcc to produce a binary program, this currently works for all code usage apart from functions.

## Assignment Seven
The Seventh assignment is to add function calls to the x86 code, I have done this using the standard Unix C calling conventions, function application, function pointers and returning functions from if statements all work correctly, however currying and partial function application do not.
