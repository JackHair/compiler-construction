#!/bin/bash
if [ $# != 1 ]; then
    echo "Usage ./compile_and_run prog.src"
    exit 0
fi
./ex.native -o -x $1 > prog.s
gcc -o prog prog.s
./prog
